package dsb

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"path/filepath"
	"strconv"
)

type Configuration struct {
	// the hostname of the dsb
	DSBHost                          string        `json:"dsb-host" yaml:"dsb-host"`
	// the port of the dsb
	DSBPort                          int           `json:"dsb-port" yaml:"dsb-port"`
	// the hostname of the mongodb database server
	MongoHost                        string        `json:"mongo-host" yaml:"mongo-host"`
	// the port of the mongodb database server
	MongoPort                        int           `json:"mongo-port" yaml:"mongo-port"`
	// the log-level which should be used.
	// logging output is only printed if the log-level is greater than the logging function level.
	// Available level names are:
	// "disable"
	// "fatal"
	// "error"
	// "warn"
	// "info"
	// "debug"
	LogLevel                         string        `yaml:"log-level" json:"log-level"`
	// prefix to be put before all logging output.
	LogPrefix                        string        `yaml:"log-prefix" json:"log-prefix"`
}

// shorthand function to combine host and port for the dsb
func (c *Configuration) DSBAddr() string {
	return c.DSBHost + ":" + strconv.Itoa(c.DSBPort)
}

// shorthand function to combine host and port for the mongodb database server
func (c *Configuration) MongoAddr() string {
	return c.MongoHost + ":" + strconv.Itoa(c.MongoPort)
}

// load configuration from a yaml file
// filename can be a relative or absolute path
func FromYAML(filename string) *Configuration {
	// get the default configuration to apply changes from the yaml file to.
	c := DefaultConfiguration()

	// get absolute path of the yaml file
	yamlAbsPath, err := filepath.Abs(filename)
	// the file does not exist
	if err != nil {
		// the golog logger does not exist yet so we have to use the default log
		log.Fatal("could not find config file")
		// still return the default config
		return c
	}
	// try to open the yaml file
	data, err := ioutil.ReadFile(yamlAbsPath)
	// panic if it fails
	if err != nil {
		panic(err)
	}
	// try to unmarshal the yaml file on top the default config and panic if it fails
	if err := yaml.Unmarshal(data, c); err != nil {
		panic(err)
	}
	// return the loaded configuration
	return c
}

// return a default configuration struct
func DefaultConfiguration() *Configuration {
	return &Configuration{
		DSBHost:                          "",
		DSBPort:                          8080,
		MongoHost:                        "",
		MongoPort:                        27017,
		LogLevel: 		                  "",
		LogPrefix:                        "\x1b[32m[DSB]\x1b[0m",
	}
}
