## An example how to use the _dsb_ package

```go
package main

import (
	"context"
	"gitlab.com/Moschcau/dsb"
	"gitlab.com/helmholtzschule/hvh-dsb/backend/modules/config"
	"os"
	"os/signal"
	"time"
)

func main() {
	// variable to hold the dsb object
	var DSB dsb.DefaultDSB
	// load the dsb config from file and initialize the dsb
	DSB.Init(dsb.FromYAML("./config/config.yml"))
	// set [DSB] prefix for the logger
	DSB.Logger().SetPrefix(DSB.Config().LogPrefix)

	DSB.Logger().Infof("| starting ...")
	DSB.Logger().Infof("| MM registering modules")
	// register all modules one by one
	// add all modules here
	DSB.RegisterModule(&config.Module{})
	// ...
	// load all registered modules
	DSB.LoadModules()

	// channel c is used to get notified on interrupt and kill signals
	c := make(chan os.Signal, 1)
	// notify on interrupt and kill
	signal.Notify(c, os.Interrupt, os.Kill)
	// goroutine for signal handling
	go func(){
		DSB.Logger().Infof("| handling interrupt and kill signal")
		// loop over signals send over channel c
		for sig := range c {
			DSB.Logger().Infof("| shutting down")
			// create timeout context
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			// shutdown the server
			_ = DSB.Server().Shutdown(ctx)
			// cancel the timeout context
			cancel()
			// handle different signals
			switch sig.String() {
			case "interrupt":
				DSB.UnregisterModules(2)
			case "kill":
				DSB.UnregisterModules(1)
			}
			return
		}
	}()

	// run dsb
	DSB.Logger().Infof("| running ...")
	DSB.Run()
}
```