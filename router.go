package dsb

import (
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
	"net/http"
	"net/url"
	"sync"
	"time"
)

// HTTP request methods
const (
	MethodGet     = "GET"
	MethodPost    = "POST"
	MethodPut     = "PUT"
	MethodDelete  = "DELETE"
	MethodConnect = "CONNECT"
	MethodHead    = "HEAD"
	MethodPatch   = "PATCH"
	MethodOptions = "OPTIONS"
	MethodTrace   = "TRACE"
)

// Context which is passed from handler to handler.
type Context struct {
	// handlers contains all request handler in order.
	handlers       []Handler
	// http request
	request        *http.Request
	// response writer pointer interface
	responseWriter http.ResponseWriter
	// the index of the current handler
	i              int
	// If true responses are handled as a json object
	// and written when all handlers where called.
	json           bool
	// pointer to the dsb struct
	dsb            DSB
	// jsonObject contains the response if json is true
	jsonObject     bson.M
	// if errors occure during the handling of the request
	// errors are stored in errors
	errors         []error
	// the http status code which is written when all handlers are done
	statusCode     int
	// a value store to pass values from handler to handler
	values         bson.M
}

// creates a new Context struct with some default values
func newContext(dsb DSB) Context {
	return Context{dsb: dsb, statusCode: 200, jsonObject: bson.M{}, json: false, values: make(map[string]interface{})}
}

// create a referece to request and response writer
func (ctx *Context) Init(w http.ResponseWriter, r *http.Request) {
	ctx.request = r
	ctx.responseWriter = w
}

// set the handler slice on the Context
func (ctx *Context) SetHandler(handlers []Handler) {
	ctx.handlers = handlers
	ctx.json = false
}

// start the execution of all handlers on the Context
func (ctx *Context) Do() {
	for ctx.i = 0; ctx.i < len(ctx.handlers); ctx.i++ {
		ctx.handlers[ctx.i](ctx)
	}
}

// skip to the next handler
func (ctx *Context) Next() {
	ctx.i++
}

// skip n handlers
func (ctx *Context) Skip(n int) {
	ctx.i += n
}

// go to the last handler
func (ctx *Context) End() {
	ctx.i = len(ctx.handlers)
}

// get the request from Context
func (ctx *Context) R() *http.Request {
	return ctx.request
}

// shorthand
// get the response writer from Context
func (ctx *Context) W() http.ResponseWriter {
	return ctx.responseWriter
}

// shorthand
// get the http GET URL parameters from the request
func (ctx *Context) Params() url.Values {
	return ctx.request.URL.Query()
}

// shorthand
// get the header from the request
func (ctx *Context) Header() http.Header {
	return ctx.request.Header
}

// shorthand
// write to the response
func (ctx *Context) Write(b []byte) (int, error) {
	return ctx.responseWriter.Write(b)
}

// marshal the temporary jsonObject and write it to the response
func (ctx *Context) writeJson(v interface{}) error {
	// try to marshal
	result, err := json.Marshal(v)
	if err != nil {
		// return possible errors
		return DSBError{Message: "could not marshal json", Code: 0}
	}
	// try to write to the response
	_, err = ctx.Write(result)
	if err != nil {
		// return possible errors
		return DSBError{Message: "could not write json", Code: 0}
	}
	// return no error if all is fine
	return nil
}

// add struct to temporay jsonObject
func (ctx *Context) AddJSON(key string, value interface{}) error {
	// if json mode is enabled add to the jsonObject
	if ctx.json {
		ctx.jsonObject[key] = value
	} else {
		// if its not enabled write to the response directly
		return ctx.writeJson(value)
	}
	return nil
}

// add a error to the Context
func (ctx *Context) Error(err error, code int) {
	// if the status code is worse the the current status code in the Context
	// save the new status code
	if ctx.statusCode < code {
		ctx.statusCode = code
	}
	// append the error to the error list
	ctx.errors = append(ctx.errors, err)
}

// set the status code on the Context
func (ctx *Context) SetStatusCode(code int) {
	ctx.statusCode = code
}

// get the value store from Context
func (ctx *Context) Values() bson.M {
	return ctx.values
}

// type definition for a handler 
type Handler func(ctx *Context)

// router contains all routes and all necessary methods for calling the right route handler.
// router also acts as the http handler trough the ServeHTTP method.
type Router struct {
	// all routes in the form routes.path.method
	routes map[string]map[string]*Route
	// mutex for locking the manipulation of the routes
	// while a route handler is active.
	mu     sync.RWMutex
	// a pointer to the DSB struct
	dsb    DSB
}

// NewRouter creates a new router struct and initializes certain values.
func NewRouter(dsb DSB) *Router {
	return &Router{
		routes: make(map[string]map[string]*Route),
		mu:     sync.RWMutex{},
		dsb:    dsb,
	}
}

// Tell the router to handle 'method' 'path' with 'handlers' using 'json' option.
func (router *Router) Handle(method string, path string, json bool, handlers ...Handler) {
	// Lock the router to modify the routes.
	router.mu.Lock()
	// make sure the router gets unlocked once finished modifying
	defer router.mu.Unlock()

	// check whether the path is already present in the router
	if _, exist := router.routes[path]; exist {
		// check whether the method on this path is already present
		if _, exist := router.routes[path][method]; exist {
			// add the handlers to the current route
			router.routes[path][method].addHandler(handlers...)
		} else {
			// create a new route struct on path and method using provided parameters
			router.routes[path][method] = &Route{
				path:     path,
				method:   method,
				json:     json,
				handlers: handlers,
			}
		}
	} else {
		// make a new map on the specified path
		router.routes[path] = make(map[string]*Route)
		// create a new route on the method sing provided parameters
		router.routes[path][method] = &Route{
			path:     path,
			method:   method,
			json:     json,
			handlers: handlers,
		}
	}
}

// shorthand to create a GET route on path using handlers
func (router *Router) Get(path string, handlers ...Handler) {
	router.Handle(MethodGet, path, true, handlers...)
}

// shorthand to create a POST route on path using handlers
func (router *Router) Post(path string, handlers ...Handler) {
	router.Handle(MethodPost, path, true, handlers...)
}

// shorthand to create a PUT route on path using handlers
func (router *Router) Put(path string, handlers ...Handler) {
	router.Handle(MethodPost, path, true, handlers...)
}

// shorthand to create a DELETE route on path using handlers
func (router *Router) Delete(path string, handlers ...Handler) {
	router.Handle(MethodDelete, path, true, handlers...)
}

// get the route for the parameters given
func (router *Router) Route(path string, method string) (*Route, error) {
	// try to get the route from the routes map-map
	route := router.routes[path][method]
	// if getting the route was successfull, return it
	if route != nil {
		return route, nil
	}
	// the route does not exist; return an error
	return nil, DSBError{Message: "route not found", Code: 404}
}

// ServeHTTP handler for the http package
func (router *Router) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// save the timestamp when the handler started
	t := time.Now()
	// create a new Context
	ctx := newContext(router.dsb)
	// initialize with request and response writer
	ctx.Init(w, r)
	// get the route matching the request
	route, err := router.Route(r.URL.Path, r.Method)
	// if no errors occured set the handlers on the Context to the handlers from the route.
	// also set json on the Context
	if err == nil {
		ctx.SetHandler(route.handlers)
		ctx.json = route.json
		// start the execution of the handlers.
		ctx.Do()
	} else {
		// an error occured; only set json to true so the error can be logged
		ctx.json = true
	}
	// set the content type of the response to json if json mode is enabled
	if ctx.json {
		w.Header().Set("Content-Type", "application/json")
	}

	// make sure the status code is within the valid bounds
	if ctx.statusCode < 100 || ctx.statusCode > 999 {
		// status code is invalid; set it to 500
		ctx.statusCode = 500
	}

	// add all errors to the json object (or write it to the end)
	_ = ctx.AddJSON("errors", ctx.errors)
	// write the json object to the response
	_ = ctx.writeJson(ctx.jsonObject)
	// If the status code is not 200, set the status code header.
	// This is to avoid the net/http package logging a warning about a 'superfluous' call to set the status code header
	if ctx.statusCode != 200 {
		w.WriteHeader(ctx.statusCode)
	}
	// log the call to the route
	router.dsb.Logger().Infof("| resp | %6s %s [%d] %s %s", time.Since(t).Round(time.Millisecond).String(), r.Method, ctx.statusCode, r.URL.Path, r.RemoteAddr)
}

// the Route type
type Route struct {
	path     string
	method   string
	json     bool
	handlers []Handler
}

// add handlers to the existing handlers i the route
func (r *Route) addHandler(handlers ...Handler) {
	for _, handler := range handlers {
		r.handlers = append(r.handlers, handler)
	}
}
