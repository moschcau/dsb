package dsb

import (
	"fmt"
)

// error struct for common errors in the dsb itself
type DSBError struct {
	Message string
	// a unique error code
	Code 	int
}

// error which occurs in a module
type ModuleError struct {
	// which module the error occurred in
	Module  string
	Message string
	// a unique error code
	Code    int
}

// a stringify method to make DSBError part of the error interface
func (err DSBError) Error() string {
	return fmt.Sprintf("[%v] hvh-dsb: %s", err.Code, err.Message)
}

// a stringify method to make ModuleError part of the error interface
func (err ModuleError) Error() string {
	return fmt.Sprintf("[%v] %s: %s", err.Code, err.Module, err.Message)
}