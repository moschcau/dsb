package dsb

// module interface defines how the module manager accesses the module
type Module interface {
	// get the name of the module
	Name() string
	// initialize the module with a reference to the module manager
	Init(manager *ModuleManager) int
	// unregister the module in case the dsb is shutdown
	Unregister(code int)
}

// the module manager struct
// module manager which is responsible for adding/removing of modules,
// initializing modules
// and calling the unregister methods on the modules in case the application is exited
type ModuleManager struct {
	// a slice of all modules
	modules []*Module
	// a pointer to the router
	// this gives modules a way to register their handlers
	router *Router
	// a reference to the dsb
	dsb DSB
}

// function to create a new module manager
func NewModuleManager(dsb DSB, router *Router) *ModuleManager {
	// return module manager with basic values
	return &ModuleManager{
		router:  router,
		dsb:     dsb,
	}
}

// register a single module in the module manager
func (mm *ModuleManager) Register(module Module) int {
	// append the module to the modules slice
	mm.modules = append(mm.modules, &module)
	// return the index of the inserted module
	return len(mm.modules) - 1
}

// unregister all modules
func (mm *ModuleManager) UnregisterAll(code int) {
	// call unregister method on every module in the modules slice
	for i := range mm.modules {
		(*mm.modules[i]).Unregister(code)
	}
}

// load all modules / starting them
func (mm *ModuleManager) LoadAll() {
	// log how many modules are registered
	mm.dsb.Logger().Infof("| MM %d modules registered", len(mm.modules))
	// loop over all modules
	for i := range mm.modules {
		// call the Init method on every module
		(*mm.modules[i]).Init(mm)
		// log the name and index of every module
		mm.dsb.Logger().Infof("| MM using %s as %d", (*mm.modules[i]).Name(), i)
	}
}

// getter for the router from the module manager
func (mm *ModuleManager) Router() *Router {
	return mm.router
}

// getter for the dsb from the module manager
func (mm *ModuleManager) DSB() DSB {
	return mm.dsb
}


