package dsb

import (
	"context"
	"github.com/kataras/golog"
	"net/http"
	"os"
)

// DSB interface declaration
type DSB interface {
	// method to get all modules loaded into the DSB
	Modules() []*Module
	// method to register a new module
	RegisterModule(module Module) bool
	// getter for the config struct
	Config() *Configuration
	// getter for the logger
	Logger() *golog.Logger
	// getter for the http server
	Server() *http.Server
	// function to pass the configuration to the dsb and initialize standard values
	Init(configuration *Configuration)
}

// default dsb type declaration
type DefaultDSB struct {
	// module manager which is responsible for adding/removing of modules,
	// initializing modules
	// and calling the unregister methods on the modules in case the application is exited
	mm     *ModuleManager
	// the config struct contains values which might change without the need to recompile the application
	config *Configuration
	// the golog logger is responsible for logging all events related to this dsb instance
	logger *golog.Logger
	// the server object calls the ServerHTTP method on the router whenever a new request is received
	server *http.Server
	// the router is responsible for managing all routes and adding of handlers
	router *Router
}

// getter for all modules of the dsb
func (dsb *DefaultDSB) Modules() []*Module {
	return dsb.mm.modules
}

// getter for the config of the dsb
func (dsb *DefaultDSB) Config() *Configuration {
	return dsb.config
}

// getter for the logger of the dsb
func (dsb *DefaultDSB) Logger() *golog.Logger {
	return dsb.logger
}

// getter for the http server
func (dsb *DefaultDSB) Server() *http.Server {
	return dsb.server
}

// shorthand function
// method for registering a new module
func (dsb *DefaultDSB) RegisterModule(module Module) bool {
	// call register on the module manager
	dsb.mm.Register(module)
	return true
}

// shorthand function
// load all modules and start them
func (dsb *DefaultDSB) LoadModules() bool {
	// call loadAll on the modules manager
	dsb.mm.LoadAll()
	return true
}

// shorthand function
// unregister all modules
// use this to make sure all database connections are closed and all files are closed
func (dsb *DefaultDSB) UnregisterModules(code int) bool {
	// call unregisterAll on the module manager
	dsb.mm.UnregisterAll(code)
	return true
}

// init passes the configuration to the dsb
// also standard values are set
func (dsb *DefaultDSB) Init(configuration *Configuration) {
	// set the configuration
	dsb.config = configuration
	// create a new logger to use
	dsb.logger = golog.New()
	// create a new router with a reference to the dsb
	dsb.router = NewRouter(dsb)
	// create a new module manager wit references to the dsb and the router
	dsb.mm = NewModuleManager(dsb, dsb.router)
	// create a new http server with the address defined in the config
	// use the router as http Handler
	dsb.server = &http.Server{
		Handler: dsb.router,
		Addr: dsb.config.DSBAddr(),
	}
}

// run is called once the dsb should start listening to requests on the host:port
func (dsb *DefaultDSB) Run() {
	// try to start the server
	err := dsb.server.ListenAndServe()
	// if it fails log it and return
	if err != nil {
		dsb.Logger().Error("Error while running the http server!")
	}
}

// method to call if the dsb should be shut down gracefully
func (dsb *DefaultDSB) Shutdown(code int, message string) {
	// log the message
	if code == 0 {
		// if the code is 0 as info
		dsb.Logger().Info(message)
	} else {
		// if the code is not 0 it's an error
		dsb.Logger().Error(message)
	}

	// log the shutdown
	dsb.Logger().Info("Shutting down server!")
	// try to shutdown the server with an timeout
	if err := dsb.server.Shutdown(context.Background()); err != nil {
		// Error from closing listeners, or context timeout:
		dsb.Logger().Error("Error while shutting down server!")
	}
	// exit the application
	os.Exit(code)
}